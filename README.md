# GitLab 🦊 features

> create a `.cvs` file of the GitLab features per edition

## Run it

```
npm start
```

## Remarks

- `x` means that the feature exists in the considered edition
- `/` means that the feature exists but partially in the considered edition