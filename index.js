const axios = require("axios")
const yaml = require("js-yaml")
const fs = require("fs")
const jsonexport = require('jsonexport')

axios({
  method: "GET",
  url: "https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/features.yml"
}).then(response => response.data)
  .then(data => {
    try {
      const content = yaml.safeLoad(data)

      //fs.writeFileSync(`./features.json`, JSON.stringify(content.features))
      
      let report = content.features.map(feature => {
        var category1, category2
        if(feature.category) {
          category1 = feature.category[0] != undefined ? feature.category[0] : ""
          if(category1=="unknown") category1 = "various"
          category2 = feature.category[1] != undefined ? feature.category[1] : ""
        } else {
          category1 = "various"
        }

        var gitlab_core, gitlab_starter, gitlab_premium, gitlab_ultimate = ""

        if (feature.gitlab_core) gitlab_core = "x"
        if (feature.gitlab_starter) gitlab_starter = "x"
        if (feature.gitlab_premium) gitlab_premium = "x"
        if (feature.gitlab_ultimate) gitlab_ultimate = "x"

        if (feature.gitlab_core=="partially") gitlab_core = "/"
        if (feature.gitlab_starter=="partially") gitlab_starter = "/"
        if (feature.gitlab_premium=="partially") gitlab_premium = "/"
        if (feature.gitlab_ultimate=="partially") gitlab_ultimate = "/"
        
        if(feature.title.includes("Maven") || feature.title.includes("NPM")) category1 = "continuous_integration"

        return {
          feature: feature.title,
          core: gitlab_core,
          starter: gitlab_starter,
          premium: gitlab_premium,
          ultimate: gitlab_ultimate,
          category1: category1,
          category2: category2
        }
      }).filter(feature => {
        return feature.core !== "" && feature.starter !== "" && feature.premium !== "" && feature.ultimate !== ""
      })

      jsonexport(report, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./features.csv`, csv)
      })

    } catch(error) {
      console.log(`😡`, error.name, error.message)
    }   
  })
  .catch(error => {
    console.log(`😡`, error.name, error.message)
  })